from enum import Enum


class Status(Enum):
    DONE = "DONE"
    CREATED = "CREATED"
    IN_PROGRESS = "IN_PROGRESS"
