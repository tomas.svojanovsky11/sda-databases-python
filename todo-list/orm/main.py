from enums import Status
from db import create_connection, create_tables

engine, session = create_connection("postgres", "123456", "etoro")
create_tables()

# Fake controller

def create_todo():
    pass


def get_todo(id: int):
    pass


def get_todos(page: int, size: int):
    pass


def delete_todo(id: int):
    pass


def change_status(id: int, status: Status):
    pass