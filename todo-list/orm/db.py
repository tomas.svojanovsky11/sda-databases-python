from sqlalchemy import create_engine, Column, Integer, Text, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


def create_connection(username: str, password: str, dbname: str):
    engine = create_engine(f"postgresql+psycopg2://{username}:{password}@localhost:5432/{dbname}", echo=True)
    Session = sessionmaker(bind=engine)
    session = Session()

    return engine, session


def create_tables():
    class Users(Base):
        __tablename__ = "users"
        id = Column(Integer, primary_key=True)
        first_name = Column(Text, nullable=False)
        last_name = Column(Text, nullable=False)
        password = Column(Text, nullable=False)
        email = Column(Text, nullable=False, unique=True)

    class Todos(Base):
        __tablename__ = "todos"

        todo_id = Column(Integer, primary_key=True)
        label = Column(Text, nullable=False)
        status = Column(Text, nullable=False)
        user_id = Column(Integer, ForeignKey(Users.id, onupdate="CASCADE", ondelete="CASCADE"), nullable=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    return Users, Todos
