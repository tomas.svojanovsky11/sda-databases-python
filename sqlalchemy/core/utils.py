from sqlalchemy import (create_engine, MetaData, Table, Column, Integer, Text, ForeignKey, Date)

def create_connection():
    engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)
    connection = engine.connect()
    metadata = MetaData()

    return engine, connection, metadata

def create_tables(metadata, engine):
    users = Table(
        'users', metadata,
        Column('user_id', Integer, primary_key=True),
        Column('email', Text, unique=True),
        Column('first_name', Text),
        Column('last_name', Text),
        Column('birthdate', Date),
        Column('password', Text),
    )

    roles = Table(
        "roles", metadata,
        Column("role_id", Integer, primary_key=True),
        Column("name", Text, unique=True, nullable=False),
    )

    user_roles = Table(
        "user_roles", metadata,
        Column("user_id", Integer, ForeignKey("users.user_id", onupdate="CASCADE", ondelete="CASCADE")),
        Column("role_id", Integer, ForeignKey("roles.role_id", onupdate="CASCADE", ondelete="CASCADE")),
    )

    metadata.drop_all(engine, tables=[user_roles, users, roles])

    roles.create(engine, checkfirst=True)
    users.create(engine, checkfirst=True)
    user_roles.create(engine, checkfirst=True)

    return user_roles, roles, users


def insert_users(connection, users):
    connection.execute(users.insert().returning(users.c.user_id, users.c.first_name), [
        {"email": "tomas.svojanovsky11@gmail.com", "first_name": "Tomas", "last_name": "Svojanovsky",
         "birthdate": "09-08-1991", "password": "gjgkglkdf"},
        {"email": "milosz@gmail.com", "first_name": "Milosz", "last_name": "Z-Man",
         "birthdate": "11-08-1994", "password": "papryka55+"},
        {"email": "endrej.babisch@gmail.com", "first_name": "Endrej", "last_name": "Babisch",
         "birthdate": "01-01-1989", "password": "liska123"}
    ])


def insert_roles(connection, roles):
    connection.execute(roles.insert(), [
        {"name": "USER"},
        {"name": "ADMIN"},
        {"name": "SUPER_ADMIN"},
    ])


def insert_user_roles(connection, users, user_roles, role_id):
    select = users.select()
    result = connection.execute(select)
    for row in result:
        connection.execute(user_roles.insert().values(role_id=role_id, user_id=row.user_id))
