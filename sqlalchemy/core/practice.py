from sqlalchemy import (create_engine, MetaData, desc)
from sqlalchemy.exc import IntegrityError
from psycopg2.errors import UniqueViolation
from errors import BadRequest

from utils import create_tables, insert_users, insert_roles, insert_user_roles

engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)

metadata = MetaData()

connection = engine.connect()

user_roles, roles, users = create_tables(metadata, engine)

insert_users(connection, users)
insert_roles(connection, roles)
select = roles.select().where(roles.c.name == "USER")
role = connection.execute(select)
role_id = role.fetchone().role_id

insert_user_roles(connection, users, user_roles, role_id)

# Task 1 - Uzivatel si chce zmenit email
# Case 1 - Vsechno probehne v poradku
# Case 2 - Email uz existuje
# Case 3 - Idecko uzivatele neexistuje

request = {
    "user_id": 1,
    "email": "milosz@gmail.com",
}

errorResponse = {
    "message": "Email already exists",
    "error_code": 400,
    "error": "EMAIL_UNIQUE_ERROR",
}


def update_user(request: dict):
    try:
        query = users.update().where(users.c.user_id == request.get("user_id")) \
            .values(email=request.get("email"))
        connection.execute(query)
    except IntegrityError as e:
        if isinstance(e.orig, UniqueViolation):
            raise BadRequest(f"Email already exists {request.get('email')}")
        else:
            raise Exception("Unknown error")


update_user(request)

# query = users.select().order_by(desc(users.c.user_id))
# result = connection.execute(query)
# for row in result:
#     print(row)

# Task 2 - Registrace uzivatele
# Case 1 - Vsechno probehne v poradku
# Case 2 - Email uz existuje

# Task 3 - Smazani uzivatele
# Case 1 - Vsechno probehne v poradku
# Case 2 - Id uzivatele neexistuje

# Task 4 - Chceme pridat uzivateli roli
# Case 1 - Vsechno probehne v poradku
# Case 2 - Uzivatel uz roli ma

# Task 5 - Chceme  uzivateli odebrat roli
# Case 1 - Vsechno probehne v poradku
# Case 2 - Mazani uzivatele ktery neexistuje