# Task 2 - Vytvor tabulku users
# Sloupce - jmeno, prijmeni, email, heslo, narozeniny

from sqlalchemy import (
    create_engine,
    Table,
    MetaData,
    Column,
    Integer,
    Text,
    String,
    Date,
    ForeignKey,
    UniqueConstraint,
    select as sqlselect,
    text,
    bindparam,
)

engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)
# dialect+driver://username:password@host:port/database

connection = engine.connect()

metadata = MetaData()

users = Table(
    "users",
    metadata,
    Column("user_id", Integer, primary_key=True),
    Column("email", Text, unique=True),
    Column('first_name', Text, nullable=False),
    Column('last_name', String(99), nullable=False),
    Column('birthdate', Date, nullable=False),
    Column('password', Text, nullable=False),
)


roles = Table(
    "roles",
    metadata,
    Column("role_id", Integer, primary_key=True),
    Column("name", Text, unique=True, nullable=False),
)

user_roles = Table(
    "user_roles",
    metadata,
    Column("role_id", Integer, ForeignKey("roles.role_id", ondelete="CASCADE", onupdate="CASCADE")),
    Column("user_id", Integer, ForeignKey("users.user_id", ondelete="CASCADE", onupdate="CASCADE")),
    UniqueConstraint("user_id", "role_id"),
)

# users.drop(engine, checkfirst=True)
# users.create(engine, checkfirst=True)
metadata.drop_all(engine, checkfirst=True)
metadata.create_all(engine, checkfirst=True)

new_user = users.insert().values(
    email="tomas.svojanovsky11@gmail.com",
    first_name="Tomas",
    last_name="Svojanovsky",
    birthdate="1991-09-08",
    password="123456"
)

result = connection.execute(new_user)
# print(result.inserted_primary_key)
# print(result.fetchall())

multiple_records = [
    {"email": "opicak@gmail.com", "first_name": "Opicak", "last_name": "Lesni", "birthdate": "09-08-1991", "password": "123456"},
    {"email": "veverka@gmail.com", "first_name": "Veverka", "last_name": "Zahradni", "birthdate": "11-11-1993", "password": "123456"}
]

new_users = users.insert().returning(users.c.user_id, users.c.first_name)
result = connection.execute(new_users, multiple_records)  # ctrl + tlacitko leve mysi

# print(result.fetchall())
for row in result.fetchall():
    print(row.user_id, row.first_name)

# USER
# ADMIN
# SUPER_ADMIN

query = roles.insert()
new_roles = [
    {"name": "USER"},
    {"name": "ADMIN"},
    {"name": "SUPER_ADMIN"},
]
connection.execute(query, new_roles)

# def get_role(name: str):
#     select = roles.select().where(roles.c.name == "USER")
#     role = connection.execute(select)
#     return role.fetchone().role_id
#
# role_id = get_role("USER")

# Cheme vybrat role_id nejake role - konkretne role USER
select = roles.select().where(roles.c.name == "USER")
role = connection.execute(select)
role_id = role.fetchone().role_id

# Vybrat vsechny uzivatele
select = users.select()
result = connection.execute(select)

for row in result:
    connection.execute(user_roles.insert().values(role_id=role_id, user_id=row.user_id))

# ctrl + shift + f vyhledani podle textu
# double shift - vyhledani souboru
# ctrl + shift + r - nahrazeni v ramci cele aplikace

u = users.alias("u")
ur = user_roles.alias("ur")
r = roles.alias("r")

user_join = u\
    .join(ur, u.c.user_id == ur.c.user_id)\
    .join(r, r.c.role_id == ur.c.role_id)

select = sqlselect([
    u.c.user_id,
    r.c.name,
    r.c.role_id
]).select_from(user_join).where(u.c.user_id == 1)

result = connection.execute(select)
print(result)

user = result.fetchone()

print(user.user, user.name, user.role_id)
print(*user)

query = text("""
    SELECT * FROM users u
    JOIN user_roles ur ON ur.user_id = u.user_id
    JOIN roles r ON r.role_id = ur.role_id
    WHERE u.user_id = :user_id
""")

query = query.bindparams(
    bindparam("user_id", 1)
)

result = connection.execute(query)

connection.close()
