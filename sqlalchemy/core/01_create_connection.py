# Task 1 - Pripojit se na databazi

from sqlalchemy import (create_engine)

engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)
# dialect+driver://username:password@host:port/database

connection = engine.connect()
connection.execute("SELECT 1")
connection.close()
