from sqlalchemy import create_engine, Column, Integer, Text, Date, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)

Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()


class Users(Base):
    __tablename__ = "users"

    user_id = Column(Integer, primary_key=True)
    email = Column(Text, unique=True, nullable=False)
    first_name = Column(Text, nullable=False)
    last_name = Column(Text, nullable=False)
    password = Column(Text, nullable=False)
    birthdate = Column(Date, nullable=False)
    roles = relationship("Roles", secondary="user_roles", back_populates="users")

    def __repr__(self):
        return f"<USER {self.user_id} {self.first_name} {self.last_name}>"


class UserRoles(Base):
    __tablename__ = "user_roles"

    user_id = Column(Integer,
                     ForeignKey("users.user_id", ondelete="CASCADE", onupdate="CASCADE"), primary_key=True)
    role_id = Column(Integer,
                     ForeignKey("roles.role_id", ondelete="CASCADE", onupdate="CASCADE"), primary_key=True)


class Roles(Base):
    __tablename__ = "roles"

    role_id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False, unique=True)
    users = relationship("Users", secondary="user_roles", back_populates="roles")


Base.metadata.drop_all(engine, checkfirst=True)
Base.metadata.create_all(engine, checkfirst=True)

# print(user.user_id)
# print(user)

users = [
    Users(
        first_name="Ferenc",
        last_name="Jahodka",
        email="fero.jahodka@gmail.com",
        password="123456",
        birthdate="1991-09-08",
    ),
    Users(
        first_name="Dzamil",
        last_name="Ovesny",
        email="dzamil.ovesny@gmail.com",
        password="123456",
        birthdate="1991-09-08",
    ),
]

session.add_all(users)
session.commit()

# for user in users:
#     print(user)

# user = session.query(Users).where(Users.user_id == 1).one()
# print(user)
# for user in users:
#     print(user)

roles = [
    Roles(name="ADMIN"),
    Roles(name="USER"),
    Roles(name="SUPER_ADMIN"),
]

session.add_all(roles)

user = Users(
    first_name="Tomas",
    last_name="Svojanovsky",
    email="tomas.svojanovsky11@gmail.com",
    password="123456",
    birthdate="1991-09-08"
)

user.roles.extend(roles)

for user in users:
    user.roles.extend(roles)


session.add(user)

users = session.query(Users).all()
for user in users:
    for role in user.roles:
        print(role.role_id)


result = session.query(Users, UserRoles, Roles)\
    .join(UserRoles, UserRoles.user_id == Users.user_id)\
    .join(Roles, Roles.role_id == UserRoles.role_id)

for row in result:
    print(row, "xxx")

session.query(Users).where(Users.user_id == 3).update({"email": "tomas.svojanovsky66@gmail.com"})

try:
    session.begin()

    # nekolik queries - insert, update nebo delete

    session.commit()
except Exception as e:
    session.rollback()
    print(e)


session.commit()
session.close()
