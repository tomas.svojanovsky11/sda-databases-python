from enum import Enum
from sqlalchemy import (
    create_engine,
    MetaData,
    Table,
    Text,
    Integer,
    Date,
    Column,
    ForeignKey,
    UniqueConstraint
)


class LandType(Enum):
    GARDEN = "GARDEN"
    CONSTRUCTION = "CONSTRUCTION"


engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/etoro", echo=True)

connection = engine.connect()

metadata = MetaData()

owners = Table(
    "owners",
    metadata,
    Column("owner_id", Integer, primary_key=True),
    Column("email", Text, unique=True, nullable=False),
    Column('first_name', Text, nullable=False),
    Column('last_name', Text, nullable=False),
    Column('birthdate', Date, nullable=False),
    Column('password', Text, nullable=False),
)

lands = Table(
    "lands",
    metadata,
    Column("land_id", Integer, primary_key=True),
    Column("area", Integer, nullable=False),
    Column("land_type", Text, nullable=False),
    # Column("address_id", Integer, ForeignKey("owners.user_id")),
    # Column("user_id", Integer, ForeignKey(owners.c.user_id)),
    # Column("address_id", Integer, ForeignKey(address.c.address_id, onupdate="CASCADE", ondelete="CASCADE")),
)

owner_lands = Table(
    "owner_lands",
    metadata,
    Column("owner_id", Integer, ForeignKey(owners.c.owner_id, onupdate="CASCADE", ondelete="CASCADE"), nullable=False),
    Column("land_id", Integer, ForeignKey(lands.c.land_id, onupdate="CASCADE", ondelete="CASCADE"), nullable=False),
    UniqueConstraint("owner_id", "land_id"),
)

address = Table(
    "address",
    metadata,
    Column("address_id", Integer, primary_key=True),
    Column("city", Text, nullable=False),
    Column("country", Text, nullable=False),
    Column("street", Text, nullable=False),  # Prikopy 7
    Column("land_id", Integer, ForeignKey(lands.c.land_id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False),
)

metadata.drop_all(engine, checkfirst=True)
metadata.create_all(engine, checkfirst=True)

connection.close()
