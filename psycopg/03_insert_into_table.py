from utils import db_connect
from psycopg2.extras import execute_batch, execute_values

connection = db_connect()

cursor = connection.cursor()

# query = """
#     INSERT INTO users(email, first_name, last_name, birthdate, password) VALUES (
# """

query = """
  INSERT INTO users(email, first_name, last_name, birthdate, password)
  VALUES %s RETURNING user_id
"""
#
# parameters = ("tomas.svojanovsky@gmail.com", "Tomas", "Svojanovsky", "1991-09-08", "123456")
# cursor.execute(query, parameters)
#
# user = cursor.fetchone()
# print(user[0])
# user = cursor.fetchone()
# print(user)
#
# query2 = """
#     INSERT INTO
#     users (email, first_name, last_name, birthdate, password)
#     VALUES (%(email)s, %(first_name)s, %(last_name)s, %(birthdate)s, %(password)s) RETURNING user_id
# """

# multiple_records = (
#     {"email": "opicak@gmail.com", "first_name": "Opicak", "last_name": "Lesni", "birthdate": "09-08-1991", "password": "123456"},
#     {"email": "veverka@gmail.com", "first_name": "Veverka", "last_name": "Zahradni", "birthdate": "11-11-1993", "password": "123456"}
# )


# cursor.executemany(query2, multiple_records)
# result = cursor.fetchone()

# while result:
#  result = cursor.fetchone()
#
#  if result is None:
#      break

query = """
  INSERT INTO users(email, first_name, last_name, birthdate, password)
  VALUES %s RETURNING user_id
"""

multiple_records = [
    ("opicak@gmail.com", "Tomas", "Svojanovsky", "1991-09-08", "123456"),
    ("lama@gmail.com", "fff", "ff", "1991-09-08", "123456"),
]


result = execute_values(cursor, query, multiple_records, fetch=True)
print(result)

connection.commit()

# cursor.execute(query, (multiple_records, ))
#
# connection.commit()
#
# print(cursor.rowcount)
# print(cursor.fetchall())
# print(cursor.fetchone())

# result = cursor.rowcount
# print(result)

cursor.close()
connection.close()

# import psycopg2
#
# connection = psycopg2.connect(
#         host="localhost",
#         database="etoro",
#         user="postgres",
#         password="123456",
#         port=5432
#     )
#
# cursor = connection.cursor()
#
# cursor.close()
# connection.close()
