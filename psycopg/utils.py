import psycopg2


def db_connect():
    return psycopg2.connect(
        host="localhost",
        database="etoro",
        user="postgres",
        password="123456",
        port=5432
    )
