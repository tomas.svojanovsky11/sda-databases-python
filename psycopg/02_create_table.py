from utils import db_connect

connection = db_connect()

cursor = connection.cursor()

cursor.execute("""
     CREATE TABLE users(
       user_id SERIAL PRIMARY KEY,
       email TEXT NOT NULL,
       first_name TEXT NOT NULL,
       last_name TEXT NOT NULL,
       birthdate DATE NOT NULL,
       password TEXT NOT NULL 
    )
""")

connection.commit()

cursor.close()
connection.close()
