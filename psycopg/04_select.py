from psycopg.utils import db_connect

connection = db_connect()

cursor = connection.cursor()

cursor.execute("""
    SELECT * FROM users
""")

print(cursor.rowcount)
print(cursor.fetchone())
print(cursor.fetchone())
print(cursor.fetchone())

# for row in cursor.fetchmany():
#     print(row)

# print(cursor.fetchall())
# print(cursor.fetchmany(50))
# print(cursor.fetchmany(50))

# print(cursor.fetchone())
# print(cursor.fetchone())

cursor.close()
connection.close()
